# prospector

comprehensive static Python code analyzer

* https://tracker.debian.org/pkg/prospector

## License
* https://github.com/PyCQA/prospector/blob/master/LICENSE

## See also
* Not yet https://pkgs.alpinelinux.org/packages?name=\*prospector\*